package main

import (
	"bytes"
	"flag"
	"os"
	"os/exec"

	"golang.org/x/sys/unix"
)

func main() {
	var envName string

	flag.StringVar(&envName, "env-file", ".env", "Environment file to take variables from")
	flag.Parse()

	environment := os.Environ()

	envFile, err := os.ReadFile(envName)
	if err != nil {
		println("Env file not found: " + err.Error())
	}

	if len(envFile) > 0 {
		environment = append(environment, readEnvironment(envFile)...)
	}

	otherArgs := flag.Args()

	commandArguments := make([]string, 1)

	switch len(otherArgs) {
	case 0:
		panic("No command set for execution")
	case 1:
	default:
		commandArguments = append(commandArguments, otherArgs[1:]...) //nolint:makezero
	}

	commandArguments[0] = conditionalLookup(otherArgs[0])

	println("Running " + commandArguments[0])

	if err := unix.Exec(commandArguments[0], commandArguments, environment); err != nil {
		panic(err)
	}
}

func conditionalLookup(original string) string {
	if original[0] == '.' || original[0] == '/' {
		return original
	}

	cmd, err := exec.LookPath(original)
	if err != nil {
		panic(err)
	}

	return cmd
}

func readEnvironment(from []byte) []string {
	split := bytes.Split(from, []byte{'\n'})
	rt := make([]string, 0, len(split))

	for _, line := range split {
		trimmed := bytes.TrimSpace(line)
		if len(trimmed) == 0 || trimmed[0] == '#' {
			continue
		}

		rt = append(rt, string(trimmed))
	}

	return rt
}
